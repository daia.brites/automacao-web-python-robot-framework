*** Settings ***
Documentation  Essa suite testa o site da magazineluiza.com.br
Resource       magalu_resources.robot
Test Setup     Abrir o navegador 
Test Teardown  Fechar o navegador

***Test Cases ***

Caso de teste 01 - Acesso ao menu "Eletrodomesticos"
    [Documentation]  Esse teste verifica o menu eletromesticos do site da magazineluiza.com
    ...              e verifica a categoria Forno
    [Tags]           menus categorias
    Acessar a home page do site magazineluiza.com.br
    Entrar no menu "Eletrodomesticos"
    Verificar se aparece a frase "Eletrodomésticos" 
    Verificar se o título da página fica "Eletrodomésticos | Magazine Luiza"
    Verificar se aparece a categoria "Forno" 
    Verificar se aparece a categoria "Adega" 

Caso de teste 02 - Pesquisa de um Produto  
    [Documentation]  Esse teste verifica a busca de um produto  
    [Tags]           busca_produtos lista_busca
    Acessar a home page do site magazineluiza.com.br
    Digitar o nome de produto "IPHONE 8 PLUS" no campo de pesquisa
    Clicar no botão de pesquisa
    Verificar o resultado da pesquisa se está listando o produto "Usado: iPhone 8 Plus 256GB Cinza Espacial Muito Bom - Trocafone" na pesquisa

# Caso de Teste 03 - Adicionar Produto no Carrinho
#     [Documentation]    Esse teste verifica a adição de um produto no carrinho de compras
#     [Tags]             adicionar_carrinho
#     Acessar a home page do site magazineluiza.com.br
#     Digitar o nome de produto "CELULAR IPHONE" no campo de pesquisa
#     Clicar no botão de pesquisa
#     Verificar o resultado da pesquisa se está listando o produto "Usado: iPhone 11 64GB Preto Muito Bom - Trocafone" na pesquisa
#     Adicionar o produto "Usado: iPhone 11 64GB Preto Muito Bom - Trocafone" no carrinho
    # Verificar se o produto "Vestido Regatão com bolço e fenda lateral" foi adicionado com sucesso
 
# Caso de Teste 04 - Remover Produto do Carrinho
#     [Documentation]    Esse teste verifica a remoção de um produto no carrinho de compras
#     [Tags]             remover_carrinho
#     Acessar a home page do site Amazon.com.br
#     Digitar o nome de produto "Xbox Series S" no campo de pesquisa
#     Clicar no botão de pesquisa
#     Verificar o resultado da pesquisa se está listando o produto "Console Xbox Series S"
#     Adicionar o produto "Console Xbox Series S" no carrinho
#     Verificar se o produto "Console Xbox Series S" foi adicionado com sucesso
#     Remover o produto "Console Xbox Series S" do carrinho
#     Verificar se o carrinho fica vazio