*** Settings ***
Documentation  Essa suite testa o site da magazineluiza.com.br
Resource       magalu_resources.robot
Test Setup     Abrir o navegador 
Test Teardown  Fechar o navegador

*** Test Cases ***

Caso de teste 01 - Acesso ao menu "Eletrodomesticos" 
        [Documentation]  Esse teste verifica o menu eletromesticos do site da magazineluiza.com
        ...              e verifica a categoria Forno
        [Tags]           menus

    Dado que estou na home page da magalizeLuiza.com.br  
    Quando acessar o menu "Eletrodomésticos"
    Então o título da página deve ficar "Eletrodomésticos | Magazine Luiza"
    E o texto "Eletrodomésticos" deve ser exibido na página
    E a categoria "forno" deve ser exibida na página


Caso de teste 02 - Pesquisa de um Produto  
    [Documentation]  Esse teste verifica a busca de um produto  
        [Tags]           busca_produtos lista_busca
    Dado que estou na home page da magalizeLuiza.com.br 
    Quando pesquisar pelo produto "IPHONE 8 PLUS"
    Então o título da página deve ficar "iphone 8 plus em Promoção no Magazine Luiza"
    E um produto da linha "IPHONE 8 PLUS" deve ser mostrado na página

