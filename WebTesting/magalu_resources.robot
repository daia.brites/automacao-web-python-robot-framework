*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${BROWSER}    chrome
${URL}     https://www.magazineluiza.com.br
${MENU_ELETRODOMESTICOS}    //*[@id="__next"]/div/main/section[1]/div[2]/header/div/div[3]/nav/ul/li[5]/div[1]/a
${HEADER_ELETRODOMESTICO}    //h1[contains(@title,'Eletrodomésticos')]
${CAMPO_PESQUISA}    //input[contains(@placeholder,'Busca no Magalu')]
${LUPA_PESQUISA}     /html[1]/body[1]/div[1]/div[1]/main[1]/section[1]/div[2]/header/div/div[2]/div[1]/div[1]/div/svg
${PRODUTO}    /html/body/div[1]/div/main/section[4]/div[5]/div/ul/li[3]/a/div[3]/h2
${CELULAR_IPHONE}    //img[contains(@cursor,'pointer')]

*** Keywords ***
Abrir o navegador
    Open Browser         browser=${BROWSER}  
    Maximize Browser Window
Fechar o navegador
    Capture Page Screenshot
    Close Browser          

Acessar a home page do site magazineluiza.com.br
    Go To    url=${URL}     
    Wait Until Element Is Visible     locator=${MENU_ELETRODOMESTICOS}

Entrar no menu "Eletrodomesticos"
    Click Element    locator=${MENU_ELETRODOMESTICOS}    

Verificar se aparece a frase "${FRASE}" 
    Wait Until Page Contains      text=${FRASE}  
    Wait Until Element Is Visible    locator=${HEADER_ELETRODOMESTICO}

Verificar se o título da página fica "${TITULO}"
    Title Should Be    title=${TITULO}

Verificar se aparece a categoria "${NOME_CATEGORIA}"    
    Element Should Be Visible    locator=(//p[@font-size='2'][contains(.,'${NOME_CATEGORIA}')])[1]

Digitar o nome de produto "${NOME_PRODUTO}" no campo de pesquisa
    Input Text    locator=${CAMPO_PESQUISA}    text=${NOME_PRODUTO}
    
Clicar no botão de pesquisa
   Press Key     ${CAMPO_PESQUISA}     \\13

Verificar o resultado da pesquisa se está listando o produto "${PRODUTO}" na pesquisa
    Wait Until Element Is Visible    locator=//h2[@data-testid='product-title'][contains(.,'${PRODUTO}')]

#GHERKIN STEPS
Dado que estou na home page da magalizeLuiza.com.br 
    Acessar a home page do site magazineluiza.com.br
    Verificar se o título da página fica "Magazine Luiza | Pra você é Magalu!"
Quando acessar o menu "Eletrodomésticos"
    Entrar no menu "Eletrodomesticos"
Então o título da página deve ficar "Eletrodomésticos | Magazine Luiza"
    Verificar se o título da página fica "Eletrodomésticos | Magazine Luiza"
E o texto "Eletrodomésticos" deve ser exibido na página
    Verificar se aparece a frase "Eletrodomésticos" 
E a categoria "forno" deve ser exibida na página
     Verificar se aparece a categoria "Forno" 

Quando pesquisar pelo produto "IPHONE 8 PLUS"
    Digitar o nome de produto "IPHONE 8 PLUS" no campo de pesquisa
    Clicar no botão de pesquisa
    Verificar o resultado da pesquisa se está listando o produto "Usado: iPhone 8 Plus 256GB Cinza Espacial Muito Bom - Trocafone" na pesquisa
    
Então o título da página deve ficar "iphone 8 plus em Promoção no Magazine Luiza"
    Verificar se o título da página fica "iphone 8 plus em Promoção no Magazine Luiza"
E um produto da linha "IPHONE 8 PLUS" deve ser mostrado na página
    Verificar o resultado da pesquisa se está listando o produto "Usado: iPhone 8 Plus 256GB Cinza Espacial Muito Bom - Trocafone" na pesquisa

Adicionar o produto "Usado: iPhone 11 64GB Preto Muito Bom - Trocafone" no carrinho
    Click Element     locator=//img[contains(@alt,'Usado: iPhone 11 64GB Preto Muito Bom - Trocafone')]
    Wait Until Element Is Visible    locator=${CELULAR_IPHONE}
    Click Element    locator=//label[@class='sc-kOHTFB brGmaU'][contains(.,'Comprar Agora')]
    Wait Until Element Is Visible    locator=//label[@class='sc-dhKdcB hnzUem sc-tagGq krlRAc sc-dnreFw hMYRSs sc-dnreFw hMYRSs'][contains(.,'Garantia Estendida Original')]
    Click Element     locator=//button[@color='secondary'][contains(.,'Agora não')]
    Wait Until Element Is Visible    locator=//div[@class='BasketPage-title'][contains(.,'Sacola')]

# Verificar se o produto "Vestido Regatão com bolço e fenda lateral" foi adicionado com sucesso
